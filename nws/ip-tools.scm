(define-module (nws ip-tools)
  #:export (get-my-ip-address get-geo-data-from-ip))

(use-modules (ice-9 receive)
             (web client)
             (web response)
             (json)
             (gnutls)
             (rnrs))

(define *ipify-root-url* "https://api.ipify.org/")
(define *ip-api-root-url* "http://ip-api.com/json/")
(define *user-agent* "NWS IP Tools")

(define (get-my-ip-address)
  "This procedure makes an HTTP request to
the ipify api (ipify.org) and responds with the current
machine's public IP address as a string if successful.
Responds with #f otherwise"
  (receive (response-status response-body)
      (http-request *ipify-root-url*
                    #:headers `((Content-Type . "application/json")
                                (User-Agent . ,*user-agent*)))
    (if (= (response-code response-status) 200)
        response-body
        #f)))

(define* (get-geo-data-from-ip ip-address #:key (use-json? #f))
  "Makes an HTTP request to the ip-api (ip-api.com)
to grab geographic data and responds with an alist as
the result. If the #:use-json? argument is #t, it will respond
with a json string instead"
  (let ((url (string-append *ip-api-root-url* ip-address)))
    (receive (response-status response-body)
        (http-request url
                      #:headers `((Content-Type . "application/json")
                                  (User-Agent . ,*user-agent*)))
      (if (= (response-code response-status) 200)
          (if use-json?
              (utf8->string response-body)
              (assoc-set!
               (json-string->scm (utf8->string response-body))
               "url"
               url))
          #f))))

